/**
 * Created by vovanz on 11/29/14.
 */
$(function() {
    $('#onPage').change(function() {
        $('#on_page_form').submit();
    });
    $('.pagination a').click(function() {
        $.ajax($(this).attr('href'), {
            success: function(data) {
                $('#content').html(data);
            }
        });
        return false;
    });
});