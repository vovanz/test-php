<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    public $layout = '//layouts/main';

    protected function beforeAction($action) {
        // Logging user actions
        Yii::app()->user->logActivity($action);

        return parent::beforeAction($action);
    }
}