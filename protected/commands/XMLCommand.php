<?php
/**
 * Created by PhpStorm.
 * User: vovanz
 * Date: 11/25/14
 * Time: 10:15 PM
 */

class XmlCommand extends CConsoleCommand {
    public function actionImport($xml_file = './xml/ekb.xml')
    {
        $xml = simplexml_load_file($xml_file);
        $products = $xml->products->product;
        foreach($products as $product) {
            $p = new Product();
            foreach($p->attributes as $name => $attribute) {
                $p->{$name} = $product->{$name}[0];
            }
            $p->name = $product->title[0];
            $p->image_url = $product->image[0];
            $p->save();
        }
    }
} 