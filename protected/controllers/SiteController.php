<?php

class SiteController extends Controller
{

    public function beforeAction($action)
    {
        Yii::app()->clientScript->registerScriptFile('//code.jquery.com/jquery-1.11.0.min.js');
        Yii::app()->clientScript->registerScriptFile('/js/script.js');
        return parent::beforeAction($action);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     * @param int $page
     */

    public function actionIndex($page = 0)
    {
        if (Yii::app()->request->getPost('onPage')) {
            Yii::app()->session['onPage'] = Yii::app()->request->getPost('onPage');
        }

        if (isset(Yii::app()->session['onPage'])) {
            $onPage = Yii::app()->session['onPage'];
        } else {
            $onPage = '10';
        }

        $products = Product::model()->findAll(array(
            'limit' => $onPage,
            'offset' => $page * intval($onPage)
        ));

        $count = Product::model()->count();
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('index', ['products' => $products, 'count' => $count, 'onPage' => $onPage]);
        } else {
            $this->render('index', ['products' => $products, 'count' => $count, 'onPage' => $onPage]);
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionProduct($id = 0)
    {
        $product = Product::model()->with('reviews')->findByPk($id);

        if (!$product) {
            throw new CHttpException(404, 'Продукт не найден');
        }

        $comments = $product->reviews;

        $comment_model = new Review();
        $comment_model->product_id = $id;

        $this->render('product', ['product' => $product, 'comments' => $comments, 'comment_model' => $comment_model]);
    }

    /**
     * Displays the contact page
     */
    public function actionReview()
    {
        $comment = new Review();
        $comment->attributes = Yii::app()->request->getPost('Review');
        if ($comment->save()) {
            if (Yii::app()->request->isAjaxRequest) {
                $comments = Review::model()->findAllByAttributes(array('product_id' => $comment->product_id));
                $comment = new Review();
                $this->renderPartial('comments', [
                    'comments' => $comments,
                    'comment_model' => $comment
                ]);
            } else {
                $this->redirect(['site/product']);
            }
        } else {
            $comments = Review::model()->with('product')->findAllByAttributes(array('product_id' => $comment->product_id));
            if (Yii::app()->request->isAjaxRequest) {
                $this->renderPartial('comments', [
                    'comments' => $comments,
                    'comment_model' => $comment
                ]);
            } else {
                $this->render('product', ['product' => $comment->product, 'comments' => $comments, 'comment_model' => $comment]);
            }
        }
    }
}