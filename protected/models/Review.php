<?php
/**
 * Created by PhpStorm.
 * User: vovanz
 * Date: 11/25/14
 * Time: 10:18 PM
 */

class Review extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('name,comment', 'required'),
            array('email', 'required'),
            array('email', 'email'),
            array('product_id', 'safe'),
            array('product', 'required'),
        );
    }

    public function tableName()
    {
        return 'review';
    }

    public function relations()
    {
        return [
            'product' => [self::BELONGS_TO, 'Product', 'product_id']
        ];
    }
} 