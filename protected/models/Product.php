<?php
/**
 * Created by PhpStorm.
 * User: vovanz
 * Date: 11/25/14
 * Time: 10:18 PM
 */

class Product extends CActiveRecord {
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'product';
    }

    public function rules()
    {
        return array(
            array('product_id', 'safe'),
            array('name', 'safe'),
            array('description', 'safe'),
            array('inet_price', 'safe'),
            array('old_price', 'safe'),
            array('price', 'safe'),
            array('reviews_num', 'safe'),
            array('image_url', 'safe'),
        );
    }

    public function relations()
    {
        return [
            'reviews' => [self::HAS_MANY, 'Review', 'product_id']
        ];
    }
} 