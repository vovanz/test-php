<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Каталог товаров',

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),

    // application components
    'preload'=>array('log'),
    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'WebUser',
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=test9',
            'schemaCachingDuration' => 3600,
            'emulatePrepare' => true,
            'username' => 'test9',
            'password' => '7a73Z83d',
            'charset' => 'utf8',
            //'enableParamLogging' => true,
            //'enableProfiling' => true,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'cache'=>array(
            'class'=>'CRedisCache',
            'hostname'=>'localhost',
            'port'=>6379,
            'database'=>0,
        ),
        'session' => array(
            'class' => 'CCacheHttpSession',
            'cacheID' => 'cache',
        ),
       /* 'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CWebLogRoute',
                    'enabled' => true,
                    'levels' => 'profile',
                    'showInFireBug' => true,
                ),
            ),
        ),*/
    ),
);