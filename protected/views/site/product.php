<?php

$this->pageTitle = 'Просмотр товара - ' . $product['name'];
?>

<div class="container product-full">
    <div class="product-image">
        <img src="<?php echo $product['image_url']; ?>"/>
    </div>
    <div class="product-data">
        <h3><?php echo $product['name']; ?></h3>

        <p>Цена:
            <?php if ($product['inet_price'] < $product['old_price']) : ?>
                <span class="old-price"><?php echo $product['old_price'] ?></span>
            <?php endif; ?>
            <span><?php echo $product['inet_price'] ?></span>
        </p>

        <p><?php echo $product['description']; ?></p>
    </div>
</div>

<div id="comments">
    <?php
    $this->renderPartial('comments', [
        'comments' => $comments,
        'comment_model' => $comment_model
    ]);
    ?>
</div>