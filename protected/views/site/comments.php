<?php if(!empty($comments)) : ?>
    <div class="container">
        <h3>Комментарии</h3>
        <?php foreach($comments as $comment) : ?>
            <div>
                <h4><?php echo $comment['name']; ?></h4>
                <p><?php echo $comment['comment']?></p>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<? $form = $this->beginWidget('CActiveForm'); ?>
<?= $form->hiddenField($comment_model, 'product_id'); ?>
    <div class="container comment-form form">
        <h3>Добавить комментарий</h3>
        <div class="row">
            <?= $form->label($comment_model, 'name')?>
            <?= $form->textField($comment_model, 'name')?>
            <p class="hint">Ведите ваше имя</p>
        </div>
        <div class="row">
            <?= $form->label($comment_model, 'email')?>
            <?= $form->textField($comment_model, 'email')?>
            <p class="hint">Ведите адрес вашей электронной почты</p>
        </div>
        <div class="row">
            <?= $form->label($comment_model, 'comment')?>
            <?= $form->textArea($comment_model, 'comment')?>
            <p class="hint">Ведите ваш комментарий</p>
        </div>
        <div class="row">
            <?=CHtml::ajaxSubmitButton('Отправить', ['site/review'], [
                'update' => '#comments'
            ])?>
        </div>
    </div>
<? $this->endWidget(); ?>