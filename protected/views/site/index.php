<?php
/* @var $products array */
/* @var $count int */
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?= CHtml::beginForm('', 'post', ["id" => "on_page_form"]); ?>
<?= CHtml::dropDownList('onPage', $onPage, ['10' => '10','20' => '20','50' => '50']); ?>
<?= CHtml::endForm(); ?>


<?php foreach($products as $product) : ?>
    <div class="container product">
        <div class="product-image">
            <img src="<?php echo $product['image_url']; ?>"/>
        </div>
        <div class="product-data">
            <a href="<?php echo $this->createUrl('product', ['id' =>  $product['id']]); ?>">
                <h3><?php echo $product['name']; ?></h3>
            </a>
            <p>Цена:
                <?php if($product['inet_price'] < $product['old_price']) : ?>
                    <span class="old-price"><?php echo $product['old_price'] ?></span>
                <?php endif; ?>
                <span><?php echo $product['inet_price'] ?></span>
            </p>
            <p>Отзывов: <?php echo $product['reviews_num']; ?></p>
            <p><?php echo $product['description']; ?></p>
        </div>
    </div>
<?php endforeach; ?>

<div class="container pagination">
    <span>Навигация: </span>
    <?php for($i = 0; $i < $count/intval($onPage); $i++) : ?>
        <a href="<?php echo $this->createUrl('index', ['page' => $i]); ?>"><?php echo ($i+1); ?></a>
    <?php endfor; ?>
</div>

<div class="container recent-activity">
    <h3>Недавние действия</h3>
    <?php foreach(Yii::app()->user->getActivity() as $activity) : ?>
        <p><?php echo $activity; ?></p>
    <?php endforeach; ?>
</div>